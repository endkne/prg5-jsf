package jsf12;

	public class Login
	{
	    private String nome;
	    private String senha;


	    public String getNome ()
	    {
	        return nome;
	    }


	    public void setNome (final String nome)
	    {
	        this.nome = nome;
	    }


	    public String getSenha ()
	    {
	        return senha;
	    }


	    public void setSenha (final String senha)
	    {
	        this.senha = senha;
	    }
	}
